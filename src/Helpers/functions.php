<?php

/**
 * @param string $key
 * @param array $value
 */
function setNotification(string $key,  array $value): void
{
    $_SESSION[$key] = serialize($value);
}

/**
 * @param $key
 */
function removeNotification($key): void
{
    unset($_SESSION[$key]);
}

/**
 * @param $key
 * @return array|null
 */
function getNotification($key): ?array
{
    return isset($_SESSION[$key]) ? unserialize($_SESSION[$key]) : null;
}

/**
 * @return bool
 */
function isAuth(): bool
{
    return isset($_SESSION['auth_user']);
}

/**
 * @return \App\Entities\User|null
 */
function getCurrentUser(): ?\App\Entities\User
{
    return (isset($_SESSION['auth_user'])) ? unserialize($_SESSION['auth_user']) : null;
}
<?php

namespace App\Helpers;

class HashPassword
{
    /**
     * @var string $solt
     */
    private string $solt = 'orew73gfg4etrcfg4xxewr347';

    /**
     * @param string $password
     * @return string
     */
    public function createHash(string $password): string
    {
        return md5($password . $this->solt);
    }

    /**
     * @param string $password
     * @param string $hash
     * @return bool
     */
    public function verify(string $password, string $hash): bool
    {
        return $hash == $this->createHash($password);
    }
}
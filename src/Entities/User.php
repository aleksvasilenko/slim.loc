<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity()
 * @ORM\Table(name="users")
 */
class User
{
    /**
     * @var int $id
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private int $id;

    /**
     * @var string $name
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private string $name;

    /**
     * @var string $last_name
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private string $last_name;

    /**
     * @var string $email
     * @ORM\Column(type="string", length=255, nullable=false, unique=true)
     */
    private string $email;

    /**
     * @var string $password
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private string $password;

    /**
     * @var string $password
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private string $created_at;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->last_name;
    }

    /**
     * @param string $last_name
     */
    public function setLastName(string $last_name): void
    {
        $this->last_name = $last_name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTime $date): void
    {
        $this->created_at = $date->format('d.m.Y');
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


}
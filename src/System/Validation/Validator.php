<?php

namespace App\System\Validation;

class Validator extends \Valitron\Validator
{
    public function __construct($data = array(), $fields = array(), $lang = null, $langDir = null)
    {
        parent::__construct($data, $fields, $lang, $langDir);
    }

    public function processValidate(array $rules)
    {
        $this->rules($rules);
        if($this->validate()) {
            return true;
        }
        return $this->errors();
    }
}
<?php

namespace App\System\View;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class View
{
    /**
     * @return Environment
     */
    public function twigViewInit(): Environment
    {
        $loader = new FilesystemLoader(ROOT_APP_PATH . '/templates');
        $twig = new Environment($loader);

        $function = new \Twig\TwigFunction('getNotification', function ($key) {
            $note = getNotification($key);
            removeNotification($key);
            return $note;
        });
        $twig->addFunction($function);
        $functionIsAuth = new \Twig\TwigFunction('isAuth', function () {
            return isAuth();
        });
        $twig->addFunction($functionIsAuth);
        $functionCurrentUser = new \Twig\TwigFunction('getCurrentUser', function () {
            return getCurrentUser();
        });
        $twig->addFunction($functionCurrentUser);
        return $twig;
    }

}
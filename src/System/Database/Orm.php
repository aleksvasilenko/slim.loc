<?php

namespace App\System\Database;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

class Orm
{
    private EntityManager $entityManager;
    private $proxyDir = null;
    private $cache = null;
    private $useSimpleAnnotationReader = false;


    public function __construct()
    {
        $conn = [
            'dbname' => 'starter_kit',
            'user' => 'root',
            'password' => '',
            'host' => 'localhost',
            'driver' => 'pdo_mysql',
        ];

        $config = Setup::createAnnotationMetadataConfiguration(
            ["src/Entities"],
            getenv("APP_DEV_MOD"),
            $this->proxyDir,
            $this->cache,
            $this->useSimpleAnnotationReader
        );

        $this->entityManager = EntityManager::create($conn, $config);
    }

    public function getEntityManager()
    {
        return $this->entityManager;
    }
}
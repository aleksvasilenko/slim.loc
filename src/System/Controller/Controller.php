<?php

namespace App\System\Controller;

use App\System\View\View;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment;

abstract class Controller
{
    protected Environment $view;

    public function __construct()
    {
        $viewObj = new View();
        $this->view = $viewObj->twigViewInit();
    }

    /**
     * @param Response $response
     * @param string $template
     * @param array $data
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function render(Response $response, string $template, array $data = []): Response
    {
        $body = $this->view->render($template, $data);
        $response->getBody()->write($body);
        return $response;
    }

    /**
     * @param array $data
     * @return JsonResponse
     */
    public function json(array $data): JsonResponse
    {
        return new JsonResponse($data);
    }

    /**
     * @param string $path
     * @param int $code
     * @param string $with
     * @return \Laminas\Diactoros\Response
     */
    public function redirect(string $path, int $code = 302): \Laminas\Diactoros\Response
    {
        $response = new \Laminas\Diactoros\Response();
        return $response
            ->withHeader('Location', $path)
            ->withStatus($code);
    }
}
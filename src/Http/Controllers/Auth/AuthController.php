<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\HashPassword;
use App\Services\UserService;
use App\System\Controller\Controller;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class AuthController extends Controller
{
    /**
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \Twig\Error\LoaderError
     */
    public function login(Request $request, Response $response, array $args = []): Response
    {
        return $this->render($response, 'auth/login.twig');
    }

    public function authenticate(Request $request, HashPassword $hp, UserService $userService): ?\Laminas\Diactoros\Response
    {
        $post = $request->getParsedBody();
        $post['password'] = $hp->createHash($post['password']);
        if($user = $userService->checkout($post['email'], $post['password'])) {
            $userService->setAuthUser($user);
            return $this->redirect('/profile');
        }
        setNotification('errors', ['Invalid value login or password']);
        return $this->redirect('login');
    }

    public function logout(): ?\Laminas\Diactoros\Response
    {
        unset($_SESSION['auth_user']);
        return $this->redirect('login');
    }
}
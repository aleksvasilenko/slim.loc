<?php

namespace App\Http\Controllers;

use App\System\Controller\Controller;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class PageController extends Controller
{
    public function index(Request $request, Response $response, array $args = []): Response
    {
        return $this->render($response, 'pages/index.twig');
    }

    public function about(Request $request, Response $response, array $args = []): Response
    {
        return $this->render($response, 'pages/about.twig', ['content' => 'Long text About Page']);
    }

    /*public function test()
    {
        return $this->json(['name' => 'Soros', 'age' => 28, 'state' => 'local']);
    }*/
}
<?php

namespace App\Http\Controllers\Register;

use App\Helpers\HashPassword;
use App\Services\UserService;
use App\System\Controller\Controller;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class RegisterController extends Controller
{
    /**
     * @param Request $request
     * @param HashPassword $hp
     * @param UserService $userService
     * @return \Laminas\Diactoros\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function register(Request $request, HashPassword $hp, UserService $userService)
    {
        $post = $request->getParsedBody();
        $post['password'] = $hp->createHash($post['password']);
        $userService->setValidator($post);
        $user = $userService->createUser($post);

        if($user) {
            setNotification('success', ['You have successfully registered. Log in to your personal account using your username and password']);
            return $this->redirect('/login');
        } else {
            setNotification('error', $user);
            return $this->redirect('/register');
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function showForm(Request $request, Response $response)
    {
        return $this->render($response, 'registration/registration.twig');
    }
}
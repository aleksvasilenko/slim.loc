<?php

namespace App\Http\Controllers\Profile;

use App\Entities\User;
use App\Services\UserService;
use App\System\Controller\Controller;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ProfileController extends Controller
{
    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function index(Request $request, Response $response): Response
    {
        if(!isAuth()) {
            return $this->redirect('login');
        }
        return $this->render($response, 'profile/index.twig');
    }
}
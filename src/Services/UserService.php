<?php

namespace App\Services;

use App\Entities\User;
use App\System\Database\Orm;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use App\System\Validation\Validator;

class UserService
{
    private User $user;
    private EntityManager $em;
    private Validator $validator;

    /**
     * @param User $user
     * @param Orm $orm
     */
    public function __construct(User $user, Orm $orm)
    {
        $this->user = $user;
        $this->em = $orm->getEntityManager();
    }

    public function setValidator(array $post): void
    {
        $this->validator = new Validator($post);
    }

    /**
     * @return array
     */
    public function getUsers(): array
    {
        return $this->em->getRepository(User::class)->findAll();
    }

    /**
     * @param array $creteria
     * @return mixed|object|null
     */
    public function getUser(array $creteria): object
    {
        return $this->em->getRepository(User::class)->findOneBy($creteria);
    }

    /**
     * @param array $data
     * @return User|array
     * @throws ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createUser(array $data): User
    {
        if($this->checkout($data['email'], $data['password']))
            return ['errors' => ['This email is using in our system']];

        $result = $this->validator->processValidate(['required' => ['email', 'password'], 'email' => ['email']]);
        dd($result);
        if( true === $result) {
            $this->user->setName($data['name'] ?? null);
            $this->user->setLastName($data['last_name'] ?? null);
            $this->user->setPassword($data['password']);
            $this->user->setEmail($data['email']);
            $this->user->setCreatedAt(new \DateTime());

            $this->em->persist($this->user);
            $this->em->flush();
            return $this->user;
        }
        return $result;
    }

    /**
     * @param string $email
     * @param string $password
     * @return mixed|object|null
     */
    public function checkout(string $email, string $password)
    {
        return $this->em->getRepository(User::class)->findOneBy(['email' => $email, 'password' => $password]);
    }

    public function setAuthUser($user)
    {
        $_SESSION['auth_user'] = serialize($user);
    }


}
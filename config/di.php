<?php

use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactory;

return [
    \Twig\Loader\FilesystemLoader::class => \DI\autowire()
        ->constructorParameter('path', 'templates'),
    \Twig\Environment::class => \DI\autowire()
        ->constructorParameter('loader', \DI\get(\Twig\Loader\FilesystemLoader::class)),
    \App\System\Database\Orm::class => \DI\autowire(),
    \App\Helpers\HashPassword::class => \DI\autowire(),
];
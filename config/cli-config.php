<?php
use App\System\Database\Orm;
$orm = new Orm();

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($orm->getEntityManager());

<?php

if(session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

define('ROOT_APP_PATH', dirname(__DIR__));

/* Upload config .ENV */
(new \DevCoder\DotEnv(ROOT_APP_PATH . "/.env"))->load();

/* Create Container */
$builder = new \DI\ContainerBuilder();

$builder->addDefinitions(ROOT_APP_PATH . "/config/di.php");

$container = $builder->build();

$app = \DI\Bridge\Slim\Bridge::create($container);

/* Set Routes */
$app->get('/', [\App\Http\Controllers\PageController::class, 'index']);

$app->get('/about', [\App\Http\Controllers\PageController::class, 'about']);

$app->get('/login', [\App\Http\Controllers\Auth\AuthController::class, 'login']);
$app->post('/login', [\App\Http\Controllers\Auth\AuthController::class, 'authenticate']);
$app->get('/logout', [\App\Http\Controllers\Auth\AuthController::class, 'logout']);

$app->get('/profile', [\App\Http\Controllers\Profile\ProfileController::class, 'index']);

$app->get('/register', [\App\Http\Controllers\Register\REgisterController::class, 'showForm']);
$app->post('/register', [\App\Http\Controllers\Register\REgisterController::class, 'register']);

$app->run();